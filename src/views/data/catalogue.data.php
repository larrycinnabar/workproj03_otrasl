<?php 
$catalogue_items = [
	'Проектирование зданий и сооружений',
	'Строительство, реконструкция, ремонт',
	'Быстровозводимые здания',
	'Дизайн, внутренняя отделка помещений',
	'Автоматизация инженерных систем',
	'Строительная, дорожная, специальная техника',
	'Нерудные строительные материалы',
	'ЖБИ и металлоконструкции',
	'БЕтон и автоклавный газобетон',
	'Строительная химия',
	'Сендвич-панели',
	'Фасады',
	'Кровельные, подкровельные материалы, кровельные работы',
	'Напольные покрытия',
	'Светопрозрачные противопожарные конструкции и противопожарное остекленение',
	'Энергоэффективные технологии и материалы'
];